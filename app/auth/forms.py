from flask.ext.wtf import Form
from wtforms import ValidationError, StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import Required, Email, Length, Regexp, EqualTo
from ..models import User

class LoginForm(Form):
	email = StringField('Email', validators=[Required(), Length(1, 64),
	Email()])
	password = PasswordField('Пароль', validators=[Required()])
	remember_me = BooleanField('Запомнить меня')
	submit = SubmitField('Войти')

class RegistrationForm(Form):
	email = StringField('Email', validators=[Required(), Length(1, 64),	Email()])
	username = StringField('Имя', validators=[
	Required(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
	'Имя пользователя должно содержать только буквы, '
	'цифры и подчеркивания')])
	password = PasswordField('Пароль', validators=[
	Required(), EqualTo('password2', message='Пароли не совпадают')])
	password2 = PasswordField('Подтвердить пароль', validators=[Required()])
	submit = SubmitField('Регистрация')

	def validate_email(self, field):
		if User.query.filter_by(email=field.data).first():
			raise ValidationError('Пользователь с этим Email уже зарегистрирован')

	def validate_username(self, field):
		if User.query.filter_by(username=field.data).first():
			raise ValidationError('Пользователь с этим именем уже зарегистрирован')