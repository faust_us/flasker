from flask.ext.wtf import Form
from wtforms import StringField, SubmitField, BooleanField, SelectField, TextAreaField
from wtforms.validators import Required, Length, Email, Regexp
from wtforms import ValidationError
from ..models import Role, User
from flask.ext.pagedown.fields import PageDownField


class CommentForm(Form):
	body = StringField('', validators=[Required()])
	submit = SubmitField('Отправить')

class PostForm(Form):
	body = PageDownField("Поделись своими мыслями", validators=[Required()])
	submit = SubmitField('Оставить след')

class EditProfileForm(Form):
	username = StringField('Имя', validators=[Length(0, 64)])
	location = StringField('Адрес', validators=[Length(0, 64)])
	about_me = TextAreaField('Обо мне')
	submit = SubmitField('Обновить')

class EditProfileAdminForm(Form):
	email = StringField('Email', validators=[Required(), Length(1, 64),	Email()])
	username = StringField('Username', validators=[
	Required(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
	'Только буквы, '
	'числа, знаки или подчеркивания')])
	confirmed = BooleanField('Подтвержден')
	role = SelectField('Права', coerce=int)
	username = StringField('Имя', validators=[Length(0, 64)])
	location = StringField('Адрес', validators=[Length(0, 64)])
	about_me = TextAreaField('Обо мне')
	submit = SubmitField('Обновить')

	def __init__(self, user, *args, **kwargs):
		super(EditProfileAdminForm, self).__init__(*args, **kwargs)
		self.role.choices = [(role.id, role.name)
			for role in Role.query.order_by(Role.name).all()]
		self.user = user

	def validate_email(self, field):
		if field.data != self.user.email and \
		User.query.filter_by(email=field.data).first():
			raise ValidationError('Email используется')

	def validate_username(self, field):
		if field.data != self.user.username and \
		User.query.filter_by(username=field.data).first():
			raise ValidationError('Имя используется')