import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
	SECRET_KEY = os.environ.get('SECRET_KEY') or b'\xb7\xaa\xc5.\x04\xbf\xbeK\x0f\x90\x9f\x0eK<\xf1\xc8N\xe3\xd0\xdc\xcc\x86\xdc`'
	SQLALCHEMY_COMMIT_ON_TEARDOWN = True
	FLASKER_MAIL_SUBJECT_PREFIX = '[Flasker]'
	FLASKER_MAIL_SENDER = 'Flasker Admin olenevodec@gmail.com'
	FLASKER_ADMIN = os.environ.get('FLASKER_ADMIN')
	FLASKER_POSTS_PER_PAGE = 15
	FLASKER_FOLLOWERS_PER_PAGE = 10
	FLASKER_COMMENTS_PER_PAGE = 10

	@staticmethod
	def init_app(app):
		pass


class DevelopmentConfig(Config):
	DEBUG = True
	MAIL_SERVER = 'smtp.googlemail.com'
	MAIL_PORT = 587
	MAIL_USE_TLS = True
	MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
	MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
	SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
	'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')


class TestingConfig(Config):
	TESTING = True
	SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
	'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')


class ProductionConfig(Config):
	SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
	'sqlite:///' + os.path.join(basedir, 'data.sqlite')



config = {
	'development': DevelopmentConfig,
	'testing': TestingConfig,
	'production': ProductionConfig,
	
	'default': DevelopmentConfig
}